package homework2;
/*
Задание 3.
Реализовать приложение, которое вычисляет среднее арифметическое элементов массива
(массив вводится с клавиатуры - по одному в каждой новой строке).
 */
import java.util.Scanner;

public class Program3
{
    public static void main(String args[])
    {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите количество чисел");
        int n = scanner.nextInt();
        int array[] = new int[n];
        double arrayAverage = 0;
        for (int i=0; i<n; i++)
        {
            System.out.println("Введите число № " + (i+1));
            array[i] = scanner.nextInt();
            arrayAverage = array[i] + arrayAverage;
        }
        arrayAverage = arrayAverage / n;

        System.out.println(arrayAverage);

    }

}


