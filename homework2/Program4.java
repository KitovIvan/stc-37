package homework2;
/*
Задание 4.
Реализовать приложение, которое меняет местами максимальный и минимальный элементы
массива (массив вводится с клавиатуры - по одному в каждой новой строке).
 */

import java.util.Arrays;
import java.util.Scanner;
import java.util.Random;


public class Program4
{
    public static void main(String args[])
    {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите количество чисел");
        int n = scanner.nextInt();
        int array[] = new int[n];
        int min = 0;
        int max = 0;
        int temp;

        for (int i = 0; i < n; i++)
        {
            System.out.println("Введите число № " + (i + 1));
            array[i] = scanner.nextInt();
        }

        for (int i = 0; i < n; i++)
        {

            if ( array[min] > array[i] )
                min = i;
            if ( array[max] < array[i] )
                max = i;
        }
        System.out.println(Arrays.toString(array));
        System.out.println("Min: "+"array["+min+"]="+array[min]);
        System.out.println("Max: "+"array["+max+"]="+array[max]);

        temp = array[min];
        array[min] = array[max];
        array[max] = temp;

        System.out.println(Arrays.toString(array));
    }
}
