package homework2;
/*
Реализовать приложение, которое выводит сумму элементов массива (массив вводится с
клавиатуры - по одному числу в каждой новой строке).
 */

import java.util.Scanner; //строка была добавлена IJ автоматически после устранения ошибки для определения типа "Scaner"-9 строка

public class Program1
{
    public static void main(String args[])
    {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите количество чисел");
        int n = scanner.nextInt();
        int array[] = new int[n];
        int arraySum = 0;
        for (int i=0; i<n; i++)          //повтор ввода кода до тех пор пока i<n и по каждой итеррации i увеличивается на 1 (i=i+1)
        {
            System.out.println("Введите число № " + (i+1));
            array[i]= scanner.nextInt();
            arraySum = arraySum + array[i];
        }
        for (int i=0; i<n; i++);
      //  arraySum = arraySum + array[i];
        System.out.println(arraySum);
    }
}
