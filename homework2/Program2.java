package homework2;
/*
Задание 2.
Реализовать приложение, которое выполняет разворот массива (массив вводится с
клавиатуры - по одному числу в каждой новой строке).
 */

import java.util.Arrays;
import java.util.Scanner;

    public class Program2
    {
        public static void main(String args[])
        {

            Scanner scanner = new Scanner(System.in);
            System.out.println("Введите количество чисел");
            int n = scanner.nextInt();
            int array[] = new int[n];

            for (int i = 0; i < n; i++)
                {
                System.out.println("Введите число № " + (i + 1));
                array[i] = scanner.nextInt();
                }

            for (int i = 0; i < n / 2; i++) //значение i должно быть от 0 середины массива
                {
                int m = array[i];               //присваиваем значение левой части заменяемой пары
                array[i] = array[n - 1 - i];
                array[n - 1 - i] = m;
                }

            System.out.println("Вывод массива в зеркальном виде "  + (Arrays.toString(array))); // выводится массив в зеркальном представлении

        }
    }
