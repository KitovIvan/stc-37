package homework3;

import java.util.Arrays;
import java.util.Scanner;

public class Program1
{
    public static void main(String[] args)
    {
       int[] massive1 = massivefill();
       massivesumm(massive1);
       int[] massive2 = massivefill();
       reverse(massive2);
       int[] massive3 = massivefill();
       middlesumm(massive3);
       int[] massive4 = massivefill();
       minmaxchange(massive4);



    }

    public static void massivesumm(int[] inputmassive)
    {
        int arraySum = 0;
        for (int i = 0; i < inputmassive.length; i++)          //повтор ввода кода до тех пор пока i<n и по каждой итеррации i увеличивается на 1 (i=i+1)
        {
            arraySum = arraySum + inputmassive[i];
        }
        System.out.println("Сумма элементов массива " + arraySum);
    }
    public static void reverse(int[] inputmassive)
    {
        int n = inputmassive.length;
        for (int i = 0; i < n / 2; i++) //значение i должно быть от 0 середины массива
        {
            int m = inputmassive[i];
            inputmassive[i] = inputmassive[n - 1 - i];
            inputmassive[n - 1 - i] = m;
        }

        System.out.println("Вывод массива в зеркальном виде " + (Arrays.toString(inputmassive))); // выводится массив в зеркальном представлении
    }
    public static void middlesumm(int[] inputmassive) {
        int n = inputmassive.length;
        double arrayAverage = 0;
        for (int i = 0; i < n; i++) {
            arrayAverage = inputmassive[i] + arrayAverage;
        }
        arrayAverage = arrayAverage / n;

        System.out.println(arrayAverage);
    }
    public static int[] massivefill()
    {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите количество чисел");
        int n = scanner.nextInt();
        int array[] = new int[n];
        for (int i = 0; i < n; i++)          //повтор ввода кода до тех пор пока i<n и по каждой итерации i увеличивается на 1 (i=i+1)
        {
            System.out.println("Введите число № " + (i + 1));
            array[i] = scanner.nextInt();
        }
        System.out.println("Введён массив" + Arrays.toString(array));
        return array;
    }
    public static void minmaxchange(int[] inputmassive)
    {
        int min = 0;
        int max = 0;
        int temp;
        int n = inputmassive.length;
        for (int i = 0; i < n; i++)

        {

            if ( inputmassive[min] > inputmassive[i] )
                min = i;
            if ( inputmassive[max] < inputmassive[i] )
                max = i;
        }
        System.out.println(Arrays.toString(inputmassive));
        System.out.println("Min: "+"array["+min+"]="+inputmassive[min]);
        System.out.println("Max: "+"array["+max+"]="+inputmassive[max]);

        temp = inputmassive[min];
        inputmassive[min] = inputmassive[max];
        inputmassive[max] = temp;

        System.out.println(Arrays.toString(inputmassive));
    }


}




