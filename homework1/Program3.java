package homework1;

import java.util.Scanner;

    public class Program3 {

        public static void main(String[] args) {
            Scanner myscanner = new Scanner(System.in);
            int total = 1; // накопитель для ответа
            int inputNumber;

            System.out.println("Введите последовательность чисел, для завершения введите 0 ");
            while (true) {
//команда считать следующую строку из консоли и считать её как целое число
                inputNumber = myscanner.nextInt();
// если введено число "0", то выход из цикла
                if (inputNumber == 0) {
                    break;
                }

//                System.out.println("Это число простое? " + isPrime(inputNumber));
  //              System.out.println("Значение накопителя до проверок: " + total);

                if (isPrime(inputNumber)) {
    //                System.out.println("Раз число простое - идем внутрь блока IF");
      //              System.out.println("Сумма цифр: " + digitsSum(inputNumber));
                    total = total * digitsSum(inputNumber);
                }

                System.out.println("Значение накопителя после проверок: " + total);
            }

            System.out.println("ОТВЕТ " + total);
        }

        /**
         * @param number число, цифры которого надо сложить
         * @return сумма цифр
         */
        public static int digitsSum(int number) {
            int result = 0;
            while (number != 0) {

                result = result + number % 10;
                number = number / 10;
            }
            return result;
        }

        /**
         * @param number число, которое надо проверить
        * @return является ли число простым
         */
        public static boolean isPrime(int number) {

            for (int i = 2; i <= number / 2; i++) {
                if (number % i == 0) {
                    return false;
                }
            }
            return true;
        }
    }





